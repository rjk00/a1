docker rm -f $(docker ps -aq)
docker rmi $(docker images -aq)
docker volume prune
docker network prune
docker system prune -a