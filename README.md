# java-poc
Proof Of Concept in Java/Spring Boot, React, MySQL

To run application:

1. In the application root directory, run build_local.sh.
2. Once build_local.sh has completed, verify that the mysql and java-poc_spring-app containers are running.
3. To create the database tables and seed data, run load_data_local.sh from the application root directory.
4. Go to "http://localhost:8081" once all the above steps have been done.
