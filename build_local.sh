docker-compose -f docker-compose-nginx.yml up --build -d
docker-compose up --build -d
docker ps -a | awk '{ print $1,$2 }' | grep nginx | awk '{print $1 }' | xargs -I {} docker stop {}
docker ps -a | awk '{ print $1,$2 }' | grep nginx | awk '{print $1 }' | xargs -I {} docker rm {}
docker-compose -f docker-compose-nginx.yml up --build -d