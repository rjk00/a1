package com.nifa.bootpoc.service;

import java.util.Optional;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nifa.bootpoc.exception.EntityNotFoundException;
import com.nifa.bootpoc.model.PlanOfWork;
import com.nifa.bootpoc.repository.PlanOfWorkRepository;

@Service
public class PlanOfWorkServiceImpl implements PlanOfWorkService{

	@Autowired
	PlanOfWorkRepository powRepository;
	
	@Override
	public Long createPlan(PlanOfWork plan) {
		PlanOfWork savedPlan = powRepository.save(plan);
		return savedPlan.getId();
	}

	@Override
	public Set<PlanOfWork> getPlans() {
		return powRepository.findAll()
				.stream()
				.collect(Collectors.toCollection(TreeSet::new));
	}

	@Override
	public PlanOfWork getPlanById(Long id) {
		Optional<PlanOfWork> plan = powRepository.findById(id);
		if(!plan.isPresent()) {
			throw new EntityNotFoundException("Could not retrieve: Plan not found for id : " + id);
		}
		
		return plan.get();
	}

	@Override
	public void updatePlan(PlanOfWork plan) {
		Long planId = plan.getId();
		if(planId != null && powRepository.existsById(planId)) {
			powRepository.save(plan);
		}else {
			throw new EntityNotFoundException("Could not update: Plan not found for id : " + planId);
		}
	}

	@Override
	@Transactional
	public void deletePlan(Long id) {
		if(id != null && powRepository.existsById(id)) {
			Optional<PlanOfWork> plan = powRepository.findById(id);
			powRepository.delete(plan.get());
		}else {
			throw new EntityNotFoundException("Could not delete: Plan not found for id : " + id);
		}
	}
}