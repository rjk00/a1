package com.nifa.bootpoc.dto.mapper;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.nifa.bootpoc.dto.PlanOfWorkDTO;
import com.nifa.bootpoc.model.PlanOfWork;
import com.nifa.bootpoc.service.PlanOfWorkService;

@Component
public class PlanOfWorkMapper {
	
	@Autowired
	private ModelMapper modelMapper;
	
	@Autowired
	private PlanOfWorkService planOfWorkService;

	public PlanOfWorkDTO converToDTO(PlanOfWork plan) {
		PlanOfWorkDTO planDTO = modelMapper.map(plan, PlanOfWorkDTO.class);
		planDTO.setNumCriticalIssues(plan.getCriticalIssues().size());
		return planDTO;
	}
	
	public PlanOfWork convertToEntity(PlanOfWorkDTO planDTO) {
		PlanOfWork plan = modelMapper.map(planDTO, PlanOfWork.class);
		
		//TODO: The way the client software is written, the user is not
		//able to update the critical issues.
		//For the time being, will be pre-populating with the existing
		//critical issues in the database, but in the future,
		//need a way for the user to update critical issues
		if(plan.getId() != null) {
			PlanOfWork oldPlan = planOfWorkService.getPlanById(plan.getId());
			plan.setCriticalIssues(oldPlan.getCriticalIssues());
		}
		return plan;
	}
}