package com.nifa.bootpoc.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PlanOfWorkDTO implements Comparable<PlanOfWorkDTO> {

	private Long id;
	@NotNull(message = "Please provide the year")
	private Integer year;
	@NotEmpty(message = "Please provide the POW status")
	@Length(max = 120)
	private String status;
	@NotEmpty(message = "Please provide a cohort")
	@Length(max = 120)	
	private String cohort;
	private String executiveSummary;
	private String meritPeerReview;
	private String stakeHolderActions;
	private String stakeHolderIdMethods;
	private String stakeHolderHowConsidered;
	private Integer numCriticalIssues;
	
	
	
	public PlanOfWorkDTO() {
		super();
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Integer getYear() {
		return year;
	}
	public void setYear(Integer year) {
		this.year = year;
	}
	public String getCohort() {
		return cohort;
	}
	public void setCohort(String cohort) {
		this.cohort = cohort;
	}
	public String getExecutiveSummary() {
		return executiveSummary;
	}
	@JsonProperty("summary")
	public void setExecutiveSummary(String summary) {
		this.executiveSummary = summary;
	}
	
	public String getMeritPeerReview() {
		return meritPeerReview;
	}
	@JsonProperty("merit_peer_review")
	public void setMeritPeerReview(String meritPeerReview) {
		this.meritPeerReview = meritPeerReview;
	}
	
	public String getStakeHolderActions() {
		return stakeHolderActions;
	}
	@JsonProperty("stakeholder_actions")
	public void setStakeHolderActions(String stakeHolderActions) {
		this.stakeHolderActions = stakeHolderActions;
	}
	
	public String getStakeHolderIdMethods() {
		return stakeHolderIdMethods;
	}
	@JsonProperty("stakeholder_methods")
	public void setStakeHolderIdMethods(String stakeHolderIdMethods) {
		this.stakeHolderIdMethods = stakeHolderIdMethods;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStakeHolderHowConsidered() {
		return stakeHolderHowConsidered;
	}
	@JsonProperty("stakeholder_how_considered")
	public void setStakeHolderHowConsidered(String stakeHolderHowConsidered) {
		this.stakeHolderHowConsidered = stakeHolderHowConsidered;
	}

	public Integer getNumCriticalIssues() {
		return numCriticalIssues;
	}
	@JsonProperty("number_of_critical_issues")
	public void setNumCriticalIssues(Integer numCriticalIssues) {
		this.numCriticalIssues = numCriticalIssues;
	}
	
	@Override
	public int compareTo(PlanOfWorkDTO o) {
		return (int)(id - o.id);
	}
}