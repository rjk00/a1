package com.nifa.bootpoc.controller;

import java.net.URI;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.web.util.UriComponentsBuilder;

import com.nifa.bootpoc.dto.PlanOfWorkDTO;
import com.nifa.bootpoc.dto.mapper.PlanOfWorkMapper;
import com.nifa.bootpoc.model.PlanOfWork;
import com.nifa.bootpoc.service.PlanOfWorkService;

@RestController
@RequestMapping("/plans")
public class PlanOfWorkController {
	
	@Autowired
    private PlanOfWorkService powService;
	
	@Autowired
	private PlanOfWorkMapper mapper;

	@GetMapping
	@ResponseStatus(HttpStatus.OK)
	public Set<PlanOfWorkDTO> getAllPlans() {
		Set<PlanOfWorkDTO> plans = powService.getPlans()
			.stream()
			.map(plan -> mapper.converToDTO(plan))
			.collect(Collectors.toCollection(TreeSet::new));
		
		return plans;
	}
	
	@GetMapping("/{id}")
	@ResponseStatus(HttpStatus.OK)
	public PlanOfWorkDTO getPlan(@PathVariable Long id) {
		PlanOfWork plan = powService.getPlanById(id);
		
		return mapper.converToDTO(plan);
	}
	
	@PostMapping
	public ResponseEntity<String> createPlan(@Valid @RequestBody PlanOfWorkDTO planDTO, UriComponentsBuilder uri) {
		//NOTE: critical issues cannot be created because there is
		//currently no way for the client to create critical issues
		
		PlanOfWork plan = mapper.convertToEntity(planDTO);
		Long createdPlanId = powService.createPlan(plan);
		
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
				.buildAndExpand(createdPlanId).toUri();
		ResponseEntity<String> response = ResponseEntity.created(location).build();
		return response;
	}
	
	@PutMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void updatePlan(@Valid @RequestBody PlanOfWorkDTO planDTO, @PathVariable Long id) {
		//NOTE: critical issues will not be updated because there is
		//currently no way for the client to update critical issues
		
		PlanOfWork plan = mapper.convertToEntity(planDTO);
		powService.updatePlan(plan);
	}
	
	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deletePlan(@PathVariable Long id) {
		powService.deletePlan(id);
	}
}