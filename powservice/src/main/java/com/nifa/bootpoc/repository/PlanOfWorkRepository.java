package com.nifa.bootpoc.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.nifa.bootpoc.model.PlanOfWork;

@Repository
public interface PlanOfWorkRepository extends JpaRepository<PlanOfWork, Long> {

}